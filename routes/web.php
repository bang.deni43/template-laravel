<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'home@home');

Route::get('/register', 'register@register');

Route::post('/welcome', 'welcome@welcome');

Route::get('/table', function(){
    return view('table.table');
});

Route::get('/data-table', function(){
    return view('table.datatable');
});